## Personalised framework

- The framework is currently tested for Tinnitus, (Diabetes is in progress!).

- The work from scratch starts from preprocess_tinnitus.py file

- Actual modelling is from the tinnitus_model.py file.

- Other Python files contain all the required functions. Ctrl+ left click to go to that particular function which has detailed documentation on its use.

- The supporting Dataframes are not available for public. 

---
