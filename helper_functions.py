import numpy as np
import pandas as pd
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.compose import ColumnTransformer
from sklearn.preprocessing import LabelEncoder, OneHotEncoder

'''
This file contains all the required common helping functions for preprocessing of data
'''

'''
Take into considerations, for weeks of data; 
Sometimes people have EOD questionnaires answered multiple times a day 
as opposed to just once every day (the ideal should be case). For them we average the data which is on the same 
day. We also can't use time delta to calculate the difference between first and last 
day and use that to compare against threshold of some days. Because a user might have a 
significant gap which would get counted in the delta and give out wrong results ie. 
having a significant gap in ema recording shouldn't account towards a user having 
large no. of sessions.
'''

#-------------------------------------------------------------------------------------

'''
Function: df_column_selector()
------------------------

Select only the required columns from the dataframe.

Parameters
----------

req_frame: The dataframe where modification is required

req_cols: Required columns

Returns
-------

modified dataframe

'''



def df_column_selector(req_frame, req_cols):
    final_cols = [col for col in req_frame.columns if col in req_cols]
    req_frame = req_frame[final_cols]
    return(req_frame)


'''
Take into considerations, for weeks of data; 
Sometimes people have EOD questionnaires answered multiple times a day 
as opposed to just once every day (the ideal should be case). For them we 
average the data which is on the same day. We also can't use time 
delta to calculate the difference between first and last day and use 
that to compare against threshold of some days. Because a user might have a 
significant gap which would get counted in the delta and give out 
wrong results ie.having a significant gap in ema recording shouldn't 
account towards a user having large no. of sessions.

Update: The work in averaging to remove duplicate EMA would be time consuming. 
Instead we can just remove the duplicate, and keep the most recent value 
of that day. The function below does that. Note: Run this before 
any major preprocessing task

TODO(): Remove duplicates, but give option of keeping mean, median values
#------------------------------------------------------------------------------

Function: remove_dup_EOD()
------------------------

Remove the multiple duplicated answered EOD answeres for each user. 
The most recently answered EOD is kept. 

Parameters
----------

duplicate : How to handle duplicate, Two ways 'remove' or 'aggregate'
For aggregate, numerical features and categorical features along with their
timestamps are required

req_frame : The dataframe where modification is required

user_col : The column where user_id are stored

date_time_col : Column where date-time is stored

num_features: numerical features with timestamps

cat_features: categorical features with timestamps

Returns
-------

modified dataframe

'''

def remove_dup_EOD(duplicate, req_frame, user_col, date_time_col, num_features = None, cat_features = None):
    # To remove duplicates 
    
    if (duplicate == 'remove'):
        missed_val = np.array([], dtype = int)
        
        for user in req_frame[user_col].unique():
            per_user_df = req_frame.loc[req_frame[user_col] == user]
        
            per_user_df_1 = per_user_df.copy()
            per_user_df_1[date_time_col] = per_user_df_1[date_time_col].dt.date
        
            per_user_df_1 = per_user_df_1.drop_duplicates(date_time_col,keep='last')
        
            missed_val = np.append(missed_val,per_user_df.index.difference(per_user_df_1.index).values)
        
        req_frame = req_frame.drop(req_frame.index[missed_val])
        req_frame = req_frame.reset_index(drop = True)
        return(req_frame)
    
    #To aggregate duplicates
    else:
        new_frame = pd.DataFrame()
        for user in req_frame['user_id'].unique():
            per_user_df = req_frame.loc[req_frame['user_id'] == user]
    
            #Get the index required to be kept
            per_user_df_1 = per_user_df.copy()
            per_user_df_1['ts'] = per_user_df_1['ts'].dt.date
            get_index = per_user_df_1.drop_duplicates('ts',keep='last').index
            
            #Calculate mean for numerical vals and replace index with get_index

            if(num_features):
            
                mean_frame_num = per_user_df[num_features].resample('D', on='ts').mean().reset_index().dropna()
                mean_frame_num['cus_val'] = get_index
                mean_frame_num.set_index('cus_val', inplace=True)
            
            #Same procedure for categorical, except take mode instead of mean
    
            if (cat_features):
                
                mean_frame_cat = per_user_df[cat_features].resample('D', on='ts').mean().reset_index().dropna()
                mean_frame_cat['cus_val'] = get_index
                mean_frame_cat.set_index('cus_val', inplace=True)
                mean_frame_cat = mean_frame_cat.round()

            #Megre two frames without duplicating
            
            if (num_features and cat_features):
    
                cols_to_use = mean_frame_cat.columns.difference(mean_frame_num.columns)
                dfNew = pd.merge(mean_frame_num, mean_frame_cat[cols_to_use], left_index=True, right_index=True, how='outer')
            
            else:
                dfNew = mean_frame_num.copy()

            dfNew['user_id'] = user
            del dfNew['ts']
            dfNew["ts"] = per_user_df["ts"].loc[dfNew.index]
            
            new_frame = pd.concat([new_frame, dfNew])
        
        new_frame  = new_frame.sort_index()
        return(new_frame)


'''
Function: remove_users()
------------------------

Remove required user-ids along with their rows from dataframe 

Parameters
----------

user_list : List of users-ids to be removed

col_name : Column where user ids are stored

usr_frame : The dataframe where modification is required

Returns
-------

Modified dataframe

'''


def remove_users(user_list, col_name, usr_frame):
    
    usr_frame = usr_frame[usr_frame[col_name].isin(user_list).__invert__()]
    return (usr_frame)


'''
Note: There is no way to find and remove them using automated way 
(IQR, extreme-values less repeated, rounding (mean+std.) to nearest 10)
so for now look at the plots of column and remove 
#----------------------------------------------------------------------

Function: outlier_identifier()
------------------------

Find if there are outliers (Based on box plot visualisation)

Parameters
----------

frame : required dataframe 

col_name_list: columns whose boxplot needs to be generated

Returns
-------

boxplot(s) for select columns 

'''

def outlier_identifier(frame,col_name_list):
    
    return(frame[col_name_list].plot.box())

    

'''
Note: The TFIDF here in our case is calculated for each user and 
not just for the column. In our work of personalized neighbourhood, 
where user is added iteratively the TFIDF needs to be calculated for each user.
#------------------------------------------------------------------------------------

Function: TFIDF_vectorizer()
----------------------------

Convert categorical variable into TFIDF vector

Parameters
----------

req_frame : Required dataframe

required_col_name : categorical column names for processing

user_col : The column where user_id are stored

Returns
-------

Modified dataframe

See Also
--------

categorical_encoder : Encode categorical variables

'''

def TFIDF_vectorizer(req_frame, required_col_name, user_col):
    colstore = []
    for user in req_frame[user_col].unique():
        per_user_df = req_frame.loc[req_frame[user_col] == user]

        temp_doc = (list(per_user_df[required_col_name].astype(str)))
        temp_doc = ' '.join(temp_doc)

        colstore.append(temp_doc)

    vectorizer = TfidfVectorizer()
    vectors = vectorizer.fit_transform(colstore[:])
    feature_names = vectorizer.get_feature_names()
    dense = vectors.todense()
    denselist = dense.tolist()
    df = pd.DataFrame(denselist, columns=feature_names)
    
    tempdf = pd.DataFrame()
    usercnt = 0
    for user in req_frame[user_col].unique():
        per_user_df = req_frame.loc[req_frame[user_col] == user]

        arr = df.iloc[usercnt, :]
        arr.index = arr.index.str.upper()
        
        per_user_df['new_val'] = per_user_df[required_col_name].map(arr)
        tempdf = pd.concat([tempdf, per_user_df])
        usercnt += 1
       
    return(tempdf)
     
'''
Function: categorical_encoder()
----------------------------

Label & one hot encode categorical variable. 

Parameters
----------

tfidf_req : There is an option to perform TFIDF on required feature which can be 
            provided. This function calculates TFIDF and multiplies its value back to
            one hot encoded columns. (See TFIDF_vectorizer())
            
required_col_name : categorical column names for processing

req_frame_cat : Required dataframe

user_col_name: Name of column where user-ids are stored

Returns
-------

Modified dataframe with encoded categorical variables

'''   

def categorical_encoder(tfidf_req, required_col_name, req_frame_cat, user_col_name): 
    
    if tfidf_req == 'yes':
        req_frame = TFIDF_vectorizer(req_frame_cat,required_col_name, user_col_name)
    else:
        req_frame = req_frame_cat.copy()
    
    categorical_feature = req_frame.columns.get_loc(required_col_name)
    le = LabelEncoder()
    le.fit(req_frame.iloc[:, categorical_feature])
    req_frame.iloc[:, categorical_feature] = le.transform(req_frame.iloc[:, categorical_feature])
        
    encoder = ColumnTransformer(
    [('one_hot_encoder', OneHotEncoder(categories='auto'), [categorical_feature])],
    remainder='passthrough')  # Leave the rest of the columns untouched
        
    X = encoder.fit(np.array(req_frame))
    
    encoded_train = pd.DataFrame(X.transform(req_frame))
        
    col_names = []
    for i in range(len(le.classes_)):    
        col_names.append(le.classes_[i])
    
    col_names = [required_col_name + s for s in col_names]
    
    actual_cols = []
    for i in range(len(req_frame.columns)):    
        actual_cols.append(req_frame.columns[i])
   
    actual_cols.remove(required_col_name)
    
    col_names = col_names + actual_cols

    encoded_train.columns = col_names
    
    encoded_train.ts = pd.to_datetime(encoded_train.ts, format="%d-%m-%Y %H:%M")
    
    col_names.remove('ts')
    
    encoded_train[col_names] = encoded_train[col_names].astype(float)
    
    if tfidf_req == 'yes':
        for tot_cols in range(len(le.classes_)):
            encoded_train.iloc[:,tot_cols] = encoded_train.iloc[:,tot_cols] * encoded_train.new_val
        
        encoded_train = encoded_train.iloc[:,:-1]
        
        '''
    
        The time chain of user is broken when we do TFIDF on the data which needs to 
        restored. What we can do is keep the users at there place according to time; 
        How it is originally in the dataframe and apply rest of the transformation. This is under 
        the assumption that no two user might have same time, precision to seconds 
        level. So essentially we are letting go of user pattern and converting to 
        original form.
    
        '''
        tempdf = pd.DataFrame()
        usercnt = 0
        for user in req_frame['user_id'].unique():

            per_user_df = req_frame_cat.loc[req_frame_cat['user_id'] == user]
            per_user_df = per_user_df.sort_values(by='ts')
            tempdf = pd.concat([tempdf, per_user_df])
            usercnt += 1
    
        encoded_train = encoded_train.set_index(tempdf.index)
        encoded_train = encoded_train.sort_index()
        
        #tempdf = tempdf.reset_index()    
        #encoded_train = encoded_train.assign(ts=tempdf['ts'])    
        #encoded_train = encoded_train.sort_values(by='ts')
    
        encoded_train = encoded_train.reset_index(drop = True)
        return(encoded_train)
    else:
        return(encoded_train)

'''
Function: user_separator()
----------------------------

Separate users into short and long based on number of days their EMA was recorded.

Parameters
----------

req_frame : Required dataframe

user_col : The column where user_id are stored

day_diff : Threshold (in number of days)

Returns
-------

List of short & long user_ids. (Can be modified to return the separated
Dataframes if required) 

'''
        
def user_separator(req_frame, user_col, day_diff,ts_feature):
    short_users = pd.DataFrame()
    long_users = pd.DataFrame()

    #ts_feature = req_frame.columns.get_loc(required_col_name)
 
    for user in req_frame[user_col].unique():
        per_user_df = req_frame.loc[req_frame[user_col] == user]
        #per_user_df = per_user_df.resample('d', on='ts').mean().dropna(how='all')
        per_user_df = per_user_df.reset_index(drop = True)
        
        #daydelta = (per_user_df.tail(1)[ts_feature] - per_user_df[ts_feature][0]).iloc[0].days
        
        if len(per_user_df) > day_diff:
            long_users = pd.concat([long_users, per_user_df])
        else:
            short_users = pd.concat([short_users, per_user_df])
    
    short_users = short_users.reset_index(drop=True)
    long_users = long_users.reset_index(drop=True)     
    
    return short_users[user_col].unique(), long_users[user_col].unique()







