import numpy as np
from sklearn.preprocessing import StandardScaler
import statsmodels.api as sm
import pandas as pd
from sklearn.metrics import mean_squared_error
from math import sqrt
import pickle
import os   
import hashlib
import math


'''
Program contains all functions required for building personalized neighbourhood

'''

'''
Function: round_down()
------------------------
Round the floating point precision, (instead of round up followed by
                                     general round() default function)

Parameters
----------

num : floating point number

digits : required precision

Return
------

rounded number

'''


def round_down(num,digits):
        factor = 10.0 ** digits
        return math.floor(num * factor) / factor


'''
Function: make_req_frame()
------------------------

Keep required user-ids along with their rows from original dataframe 

Parameters
----------

user_list : List of users-ids to be kept

usr_frame : The dataframe where modification is required

Return
------

Modified dataframe
'''

def make_req_frame(req_frame, user_arr):
    return(req_frame[req_frame['user_id'].isin(user_arr)])

'''
Function: lagged_df()
------------------------

Convert timeseries series to supervised frame, based on lag value

Parameters
----------

req_frame : The dataframe where modification is required

lag_val : number of lag in time-point

lag_var : feature on which lag is required

Return
------

Lagged dataframe

'''

def lagged_df(req_frame, lag_val, lag_var):
    lagged_frame = req_frame[:-lag_val]
    lagged_frame['lagged'] = np.array(req_frame[lag_var][lag_val:])
    return lagged_frame

'''
Function: normalizer()
------------------------

Normalize the variables using sklearn's standard scaler

Parameters
----------

trainX : Training data for fit_transform

testX : Test data for transform

Return
------

normalized dataframes

'''

def normalizer(trainX, testX):
    
    std_scaler = StandardScaler()
    fitter = std_scaler.fit(trainX)
    normal_train = fitter.transform(trainX)
    normal_test = fitter.transform(testX)
    
    #return normal_train, normal_test
    return pd.DataFrame(normal_train), pd.DataFrame(normal_test) 

'''
Function: create_model()
------------------------

Create learning model on the data. (Currently used statsmodel Ordinary least squares)

Parameters
----------

dt_model_train : training data

dt_model_lbl : labels of training data

column_ignore : Columns like user_id or timestamps to ignore when fitting data to learning
                model
                
Return
------

Fitted model

'''


def create_model(dt_model_train, dt_model_lbl,column_ignore):
    
   
    dt_model_train = dt_model_train[[col for col in dt_model_train.columns if col not in column_ignore]]
    
    dt_model_train = sm.add_constant(dt_model_train, prepend = False, has_constant='add')
   # normal_test = sm.add_constant(normal_test, prepend = False)

    dt_model_train = dt_model_train.reset_index(drop = True) 
    dt_model_lbl = dt_model_lbl.reset_index(drop = True) 
    
    #print(dt_model_lbl)
    m = sm.OLS(dt_model_lbl, dt_model_train)

    results = m.fit()

    return results

'''
Function: add_user_rmse()
------------------------

Add or remove user from the learning model based on selection criteria and alpha threshold.
In detail, We make this metric dynamic for each user. The selection criteria follows the below 
steps;

1. If curr_error > previous error,
   Calculate increment = (current error - previous error) / previous error. 

2. How much this error has increased? increment 

3. To control, permissible_increment = current error*alpha + current error.

4. if previous+increment > permissible_increment, discard user. else allow

Parameters
----------

current_rmse : current error of learning model

previous_rmse : previous error 

discovery_method : Choose between 'exhaustive_search' or 'early_termination' for 
                   neighbourhood discovery
            
get_status : status which helps in breaking loop. (As part of early_termination logic)

Return
------

Status value for decision of the loop

'''


def add_user_rmse(current_rmse, previous_rmse, discovery_method, get_status, alpha):
        
        
        #current_rmse = int((int(current_rmse*100)/float(100)) * 100)
        #previous_rmse = int((int(previous_rmse*100)/float(100)) * 100)
        
        current_rmse = round_down(current_rmse,5)
        previous_rmse = round_down(previous_rmse,5)
        
        #print(current_rmse, previous_rmse)
        print(previous_rmse, current_rmse)
        #print(current_rmse >= previous_rmse)
        #print(current_rmse >= previous_rmse + alpha)
        
        
        if previous_rmse < 0.1:
            return('break')
        
        if (current_rmse >= previous_rmse):
            
            err_incr = ((current_rmse - previous_rmse) / previous_rmse)*100
            
            err_incr = round_down(err_incr, 5)
            
            permi_incr = 100*alpha
            
            #get_status = 1
            print(err_incr, permi_incr)
            if (err_incr >= permi_incr):
                
                print('nope!')
                print('end')
                print('\n')
                
                if (discovery_method == 'exhaustive_search'):
                    
                    return(1)
                else:
                    return('break')
            print('yes!')
            print('end')
            print('\n')
            
        #print(get_status)
        return get_status
  
# =============================================================================
# 
# CACHE_PATH = r'.cached_models'
# 
# def save_model(learned_model, sorted_neighbours):
#     
#     sorted_neighbours.sort()
#     hash_strng = ''.join(str(e) for e in sorted_neighbours).encode()
#     hash_strng = hashlib.sha1(hash_strng).hexdigest()[:35]
#     
#     filename = os.path.join(CACHE_PATH, 'model_usrs_{}.pickle'.format(hash_strng))
#     
#     with open(filename, 'wb') as file:
#         pickle.dump(learned_model, file)
# 
# def search_model(sorted_neighbours):
#     
#     sorted_neighbours.sort()
#     hash_strng = ''.join(str(e) for e in sorted_neighbours).encode()
#     hash_strng = hashlib.sha1(hash_strng).hexdigest()[:35]
#     filename = os.path.join(CACHE_PATH, 'model_usrs_{}.pickle'.format(hash_strng))
#     
#     if os.path.exists(filename):
#             with open(filename, 'rb') as file:
#                 result = pickle.load(file)
#             return result
#     else:
#         return None
#     
# =============================================================================

'''
Function: predict_search()
------------------------

function which takes learning model and predicts on test data and also reframes the training data
based on next neighbour's usuefulness in the model. (intrinsically used by personal_neighbourhood
function)

Parameters
----------

discovery_method : neighbourhood building method; 'exhasutive_search' or 'early_termination'

req_test_data : test data for learned model
    
label_test : true values of test data
    
learned_model : model on long_users
    
simi_comp_vals : values used for computation in similariy (used for ignoring these values 
                                                           when calculating RMSE to account
                                                           for bias)
 
user_order : Order of long_users based on descending values of similarity

neighbours : Array of long users from training data
    
rmse_matrix : array which stores error of learned models
    
prediction_val_matrix : array which stores predicted values of learned models 
    
column_ignore : Columns like user_id or timestamps to ignore when predicting data from learned
                model
                
Returns
-------

Error array, predicted values array, list of users in training data and status value

See Also
--------

def personal_neighbourhood()


'''


def predict_search(discovery_method, req_test_data, label_test, learned_model, simi_comp_vals, user_order,neighbours, rmse_matrix, prediction_val_matrix, column_ignore, alpha):
    
    req_test_data = req_test_data[[col for col in req_test_data.columns if col not in column_ignore]]
    req_test_data = sm.add_constant(req_test_data, prepend = False,has_constant='add')
    predicted_vals = learned_model.predict(req_test_data)
    get_status = 0
    if user_order == 0:
        
        #train_user_arr = []
        #TODO(): Also get the RMSE for values not used in computation.
        rmse_matrix.append(sqrt(mean_squared_error(label_test[simi_comp_vals:], predicted_vals[simi_comp_vals:])))
        prediction_val_matrix.append(predicted_vals)
        #train_usr_arr.append(train_user)
        
       
    else:
        temp_rmse = sqrt(mean_squared_error(label_test[simi_comp_vals:], predicted_vals[simi_comp_vals:]))
        #print(temp_rmse)
        
        get_status = add_user_rmse(temp_rmse, rmse_matrix[-1], discovery_method, get_status, alpha)
        
        if (get_status == 0):
            rmse_matrix.append(sqrt(mean_squared_error(label_test[simi_comp_vals:], predicted_vals[simi_comp_vals:])))
            prediction_val_matrix.append(predicted_vals)
        else:
            neighbours.remove(neighbours[-1])
        
    return (rmse_matrix, prediction_val_matrix, neighbours, get_status)
   
    
'''
Function: data_normalizer()
------------------------

Preprocesses dataframe for applying normalization

Parameters
----------

ignore_vars_arr : list of variables (categorical) which needs to be ignored in normalising

req_frame_train, req_frame_test: training and test data

Returns
-------

normalized dataframes
 
'''


def data_normalizer(ignore_vars_arr, req_frame_train, req_frame_test):
    
    #hold_data_train = pd.DataFrame(req_frame_train[ignore_vars_arr])
    #hold_data_train = hold_data_train.reset_index(drop = True) 
    
    #hold_data_test = pd.DataFrame(req_frame_test[ignore_vars_arr])
    #hold_data_test = hold_data_test.reset_index(drop = True) 
       
    normalize_col_vars = [col for col in req_frame_train.columns if col not in ignore_vars_arr]
    
    req_frame_train, req_frame_test = normalizer(req_frame_train[normalize_col_vars],
                            req_frame_test[normalize_col_vars])

    req_frame_train.columns = normalize_col_vars
    req_frame_test.columns = normalize_col_vars
    
    #req_frame_train = pd.concat([req_frame_train, hold_data_train], axis = 1)
    #req_frame_test = pd.concat([req_frame_test, hold_data_test], axis = 1)
   
    return req_frame_train, req_frame_test


'''
Function: baseline()
------------------------

Creates a baseline (for benchmark) where all long users are taken for learned model and tested on required 
test user.

Parameters
----------

processed_frame : Training dataset containing all users

test_set : Test data of selected user

label_test : true values of test data

gap_vals : values used for computation in similariy (used for ignoring these values 
                                                           when calculating RMSE to make sure
                                                           that data used in training doesn't leak
                                                           into prediction)

similarity_matrix : matrix which contains pair of users along with their similarity metric

ignore_vars_arr, ignore_for_model : ignore required variables as required by different functions

Returns
-------

Error of the predicted values

'''     

def baseline(processed_frame, test_set, label_test, gap_vals, similarity_matrix, ignore_vars_arr, ignore_for_model, y_variable):
    
    train_data = make_req_frame(processed_frame, similarity_matrix['long_user_id'])
    train_data = lagged_df(train_data, 1, y_variable)
    label_train = pd.DataFrame(train_data['lagged'])
    label_train = label_train.reset_index(drop = True)
    train_data = train_data.drop(['lagged'], axis=1)
    
    normal_train, normal_test = data_normalizer(ignore_vars_arr, train_data, test_set)
      
    #print(normal_train.columns)
    #print(normal_test.columns)
    
    model = create_model(normal_train, label_train,ignore_for_model)
    #req_test_data = sm.add_constant(normal_test.iloc[:,:-2], prepend = False,has_constant='add')
    req_test_data = sm.add_constant(normal_test, prepend = False,has_constant='add')
  
    predicted_vals = model.predict(req_test_data)
  
    rmse = sqrt(mean_squared_error(label_test[gap_vals:], predicted_vals[gap_vals:]))
 
    return rmse   

'''
Function: personal_neighbourhood()
------------------------

Create a automated framework for personalised neighbourhood for required user. Uses all other 
functions defined in this file.

Paramters
---------

short_usr : user-id on which personalised neighbourhood is required

similarity_matrix : matrix which contains pair of users along with their similarity metric

processed_frame : Training dataset containing all users

discov_method : neighbourhood building method; 'exhasutive_search' or 'early_termination'

ignore_for_normalization : Columns (categorical and meta-data) which is required to be ignored
                            for normalisation process (See data_normalizer function)
        
ignore_for_model : Meta-data column which are not required for model building. 

y_variable : Takes string for y-variable, which is to be predicted


Returns
-------

rmse_array : array of RMSE error 

train_usr_arr : neighbours for the user

baseline_rmse : Baseline RMSE

'''

def personal_neighbourhood(short_usr, similarity_matrix, processed_frame, discov_method, ignore_for_normalization, ignore_for_model, y_variable, alpha):

    #Extract data of required short user from computed similarity matrix
    usr_similar_frame = similarity_matrix.loc[similarity_matrix['short_user_id'] == short_usr]
    
    #sort the similar user list in similarity descending order
    usr_similar_frame = usr_similar_frame.sort_values('similarity', ascending=False)
    
    #Limit all similarity user pairs whose similarity value is equal or above 80 percent
    usr_similar_frame = usr_similar_frame.loc[usr_similar_frame['similarity'] >= 0.80]
    usr_similar_frame = usr_similar_frame.reset_index(drop = True)
    
    #Get the gap(how many values used for similarity computation)
    gap_vals = usr_similar_frame['gap_values'][0]
     
    #Make Test data into required frame
    test_data = make_req_frame(processed_frame, [short_usr])
    test_data = lagged_df(test_data, 1, y_variable)
    label_test = pd.DataFrame(test_data['lagged'])
    label_test = label_test.reset_index(drop = True)
    test_data = test_data.drop(['lagged'], axis=1)
        
    #ignore_for_normalization = ['eod04DONTKNOW', 'eod04HYPER', 'eod04HYPO', 'eod04NO', 'user_id', 'ts']
    #ignore_for_model = ['user_id', 'ts']
    
    #Calculate Baseline RMSE
    
    baseline_rmse = baseline(processed_frame, test_data, label_test, gap_vals, similarity_matrix.copy(), ignore_for_normalization, ignore_for_model, y_variable)

    train_usr_arr = []
    for usr_range in range(len(usr_similar_frame)):
        
        # Add a new neighbour (here, long_user) and make it's traning data frame
        new_neighbour = usr_similar_frame['long_user_id'][usr_range]
        train_usr_arr.append(new_neighbour)
        train_data = make_req_frame(processed_frame, train_usr_arr)
        train_data = lagged_df(train_data, 1, y_variable)
        label_train = pd.DataFrame(train_data['lagged'])
        label_train = label_train.reset_index(drop = True)
        train_data = train_data.drop(['lagged'], axis=1)
  
        # Normalize the data and create a model
  
        normal_train, normal_test = data_normalizer(ignore_for_normalization, train_data, test_data)
        
        model = create_model(normal_train, label_train,ignore_for_model)
        
        #if (search_model(train_usr_arr.copy()) == None):
        
        #    model = create_model(normal_train, label_train,ignore_for_model)
        #else:
            
         #   model = search_model(train_usr_arr.copy())
          #  print("yeah!")
         
        # Create required empty arrays, at the beginning (first neighbour)  
        if usr_range == 0:
            rmse_array = []
            predicted_val_array = []
        
        rmse_array, predicted_val_array, train_usr_arr, get_status = predict_search(discov_method,                    
                                                                                      normal_test, 
                                                                                      label_test,
                                                                                      model, gap_vals, 
                                                                                      usr_range, train_usr_arr.copy(), 
                                                                                      rmse_array.copy(), 
                                                                                      predicted_val_array.copy(),
                                                                                      ignore_for_model, alpha)
        
        #if (get_status == 0 and search_model(train_usr_arr.copy()) == None):
            
         #   save_model(model, train_usr_arr.copy())
        
        
        if(get_status == 'break'):
            
            break
        
    return(rmse_array, train_usr_arr, baseline_rmse, len(label_test[gap_vals:]),len(train_usr_arr))




