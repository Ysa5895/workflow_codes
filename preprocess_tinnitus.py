'''
Preprocess the tinnitus dataset for our required framework
'''

import pandas as pd
import numpy as np
import helper_functions
from matplotlib import pyplot as plt
import missingno as msno


'''

Required columns (for dynamic data)
re_cols = ['user_id','save','question1', 'question2', 'question3', 'question4', 'question5', 
           'question6', 'question7', 'question8']

Consider length of users, discard who are less than certain 

re_cols, defines all columns required
nu_cols, defines all numeric columns (which needs to converted to numeric 
                                      datatype)

'''

re_cols = ['user_id','save','question1', 'question2', 'question3', 'question4', 'question5', 
           'question6', 'question7', 'question8']

nu_cols = ['user_id','question1', 'question2', 'question3', 'question4', 'question5', 
           'question6', 'question7', 'question8']


tinnitus_main = pd.read_csv(r'C:\Users\ysa pc\Desktop\internship\HiWi\Thesis\codes\datasets\Tinnitus/TYT_try.csv')

tinnitus_main = helper_functions.df_column_selector(tinnitus_main.copy(), re_cols)

msno.matrix(tinnitus_main)


'''
- We still have a healthy number of rows remaining after dropping NA rows
- Convert timestamp to datetime 
'''
tinnitus_main = tinnitus_main.dropna()
tinnitus_main.save = pd.to_datetime(tinnitus_main.save)    

tinnitus_main[nu_cols] = tinnitus_main[nu_cols].apply(pd.to_numeric, errors='coerce')

'''
In total there are 2332 user-ids, not all of them actual users. So first we
count their values and drop every user which has less than 10 sessions.

'''

grouped = tinnitus_main['user_id'].value_counts()
grouped = grouped.reset_index()
grouped = grouped.loc[grouped['user_id'] <= 10]
tinnitus_main = helper_functions.remove_users(grouped['index'].unique(), 'user_id',tinnitus_main)


# Sound level has the most number of outliers 
# =============================================================================
# 
# helper_functions.outlier_identifier(tinnitus_main, ['question1', 'question2', 'question3', 'question4', 'question5', 
#            'question6', 'question7', 'question8', 'soundlevel'])
# 
# tinnitus_final = helper_functions.df_column_selector(tinnitus_main.copy(), re_cols[:-1])
# 
# =============================================================================
'''
Basic preprocessing is complete save a copy before moving forward
'''

tinnitus_main.to_csv(r'datasets\Tinnitus/TYT_workcp.csv')

#----------------------------------------------------------------------------

tinnitus_final = pd.read_csv(r'datasets\Tinnitus/TYT_workcp.csv')

# Supply format string for datetime conversion, 
#to make it faster for larger datasets

tinnitus_final.ts = pd.to_datetime(tinnitus_final.ts, 
                                   format="%d-%m-%Y %H:%M")

#Remove duplicates, agrregate values

tinnitus_final_rem = helper_functions.remove_dup_EOD('remove', tinnitus_final, 'user_id', 'ts')

'''
We will have another set of users to remove since the duplicated data
is removed and a single EOD is established for every day for each 
user-id. We remove all users who have less than 10 sessions of data.

Total we have 679 users remaining, From that we now have 487 user-ids 
remaining. 
'''

grouped = tinnitus_final_rem['user_id'].value_counts()
grouped = grouped.reset_index()
grouped = grouped.loc[grouped['user_id'] <= 10]

tinnitus_remaining_data = helper_functions.remove_users(grouped['index'].unique(),
                                       'user_id', tinnitus_final_rem)

tinnitus_remaining_data = tinnitus_remaining_data.reset_index(drop =True)

tinnitus_remaining_data['user_id'].nunique()

'''
We now compute the similarities between every user pair and create 
a required frame.
'''

from similarity_framework import similaity_frame_engineering, similarity_computation

'''
We are not using any of the categorical column (question 1 and 8) for
any steps in entire framework (neither similarity computation, nor model)

We calculate similarity on all numerical variables so prepare required
frame in the following way. Since the current function only takes one
column at a time. Run a loop, (Its easy fix, but don't want to mess 
                               for now')
'''

# Calculate similarity matrix

numeric_cols = ['question2', 'question3', 'question4', 'question5', 'question6', 'question7']

final_gap_frame = pd.DataFrame()

for colnames in numeric_cols:

    if (colnames == 'question2'):
        
        gap_frame = similaity_frame_engineering('data', tinnitus_remaining_data, colnames, [],)
    
        final_gap_frame = pd.concat([final_gap_frame, gap_frame], axis = 1)
        
    else:
        gap_frame = similaity_frame_engineering('data', tinnitus_remaining_data, colnames, [],)
        
        del gap_frame['user_id']
        
        final_gap_frame = pd.concat([final_gap_frame, gap_frame], axis = 1)
        
'''
We need to find list of short and long users. We have threshold for 
long user that no. of session should be atleast 28

Now we have dataframe in place, We can calculate the similarity value
taking all the columns. We are using first 5 values. 
'''
shrt_usr_list, lng_usr_list = helper_functions.user_separator(final_gap_frame,
                                                              'user_id',28, 'ts')

similar_frame = similarity_computation(final_gap_frame, 5, 'first', shrt_usr_list, lng_usr_list, numeric_cols)

'''
These dataframes take alot time to compute so its better we save them.
'''

tinnitus_remaining_data.to_csv(r'datasets\Tinnitus/catego_tfidf_nonenco.csv')

similar_frame.to_csv(r'datasets\Tinnitus/similarity_frame_28sessions_5vals_first_all_numeric_.csv')






