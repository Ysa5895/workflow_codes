import pandas as pd
from scipy import spatial      
import numpy as np



'''
All functions which help in similarity computation and related visualisations
'''

'''
Function: similarity_computation()
----------------------------

Compute the distance between vectors, for similarity metric.

Parameters
----------

req_frame : Take the processed frame which contains features on which similarity metric is
            to be computed.
            
vector_length: Number of data_points in each feature, to be considered for similarity calculation

gap_position : Select which part of dataframe should be used for similarity computation.
               First/ Most recent 
 
shrt_usr_list, lng_usr_list : User-ids belonging to short_users and long_users respectively 
                              (See user_separator from helper_functions.py)
                              
gap_cal_columns : List of features to be used for similarity computation

Returns
-------

A similarity matrix which has similarity metric calculated for each pair of long-short user.
Along with addtional information of vector_length and part of data used.
'''

def similarity_computation(req_frame, vector_length, gap_position, shrt_usr_list, lng_usr_list, gap_cal_columns):
    
    similarity_compa = pd.DataFrame()
    
    if (gap_position == 'first'):
        
        data = []
        for users_short in shrt_usr_list:
            
            short_gap_frame = req_frame.loc[req_frame['user_id'] == users_short]
            short_gap_frame = np.array(short_gap_frame[:vector_length][gap_cal_columns]).flatten()
    
            for users_long in lng_usr_list:
                
                long_gap_frame = req_frame.loc[req_frame['user_id'] == users_long]
                long_gap_frame = np.array(long_gap_frame[:vector_length][gap_cal_columns]).flatten()
                
                simi = 1 - spatial.distance.cosine(short_gap_frame, long_gap_frame)
                
                data.append([users_short, users_long, simi, vector_length, gap_position])
                
        similarity_compa = similarity_compa.append(data)

    else:
        
        data = []
        vector_length = vector_length*-1
        
        for users_short in shrt_usr_list:
            
            short_gap_frame = req_frame.loc[req_frame['user_id'] == users_short]
            short_gap_frame = np.array(short_gap_frame[:vector_length][gap_cal_columns]).flatten()
            
            for users_long in lng_usr_list:
                
                long_gap_frame = req_frame.loc[req_frame['user_id'] == users_long]
                long_gap_frame = np.array(long_gap_frame[:vector_length][gap_cal_columns]).flatten()
                
                simi = 1 - spatial.distance.cosine(short_gap_frame, long_gap_frame)
                
                data.append([users_short, users_long, simi, vector_length, gap_position])
                
        similarity_compa = similarity_compa.append(data)
        similarity_compa.iloc[:,3] = similarity_compa.iloc[:,3] * -1  

    similarity_compa.columns = ['short_user_id','long_user_id','similarity', 'gap_values', 'gap_position']

    return similarity_compa
    
'''
Function: similaity_frame_engineering()
----------------------------

A function which creates required dataframe to be used for computation of similarity metric. 
It will take each user and according to parameters will generate required dataframe.

Parameters
----------

measure_type : Developed measure which uses different features of user for computing similarity.
               Options between 'data' : actual  data  submitted  by  the  users
                               'meta-data': metadata  of  their  user-app  interactions
               
                More info in paper (Love thy Neighbours: A Framework for Error-Driven Discovery 
               of Useful Neighbourhoods for One-Step Forecasts on EMA data) 

original_frame : Actual data_frame

data_col_of_intrst : Main feature on which similarity is dependent (From the original_frame)

col_name_arrays : Feature names from the supporting data-frame whose information would be
                  summarised between each time vectors of data_col_of_intrst. 
                  Dependent on support_frame. Leave it as empty array [], if no support_frame
                  is provided. support_frame:None

support_frame : Other dataframe whose features provide support to data_col_of_intrst in
                similarity_computation. Default:None if no such frame is provided
                
Returns
-------

A processed data-frame which is to be used for similarity computation 
(see similarity_computation)

Note
----


'''

def similaity_frame_engineering(measure_type, original_frame, data_col_of_intrst, col_name_arrays, support_frame = None):
    
    if (measure_type == 'data'):
        if not data_col_of_intrst:
            print("please provide variable of interest for data_similarity")
            return None
    
    if col_name_arrays:
     
        gapdf = pd.DataFrame()
        for col_name_from_supp_frame in col_name_arrays:
            final_temp_gap = pd.DataFrame() 
            if (measure_type == 'meta'):
                columns = ['user_id', 'gap_{}'.format(col_name_from_supp_frame)]
            else:
                columns = ['user_id', 'avg_{}'.format(col_name_from_supp_frame)]
                
            for user in original_frame['user_id'].unique():
        
                per_user_df = original_frame.loc[original_frame['user_id'] == user]
                supp_per_user_df= support_frame.loc[support_frame['user_id'] == user]
        
                per_user_df = per_user_df.reset_index(drop = True)
                supp_per_user_df = supp_per_user_df.reset_index(drop = True)
            
                per_user_df['ts'] = pd.to_datetime(per_user_df['ts']).dt.date
                supp_per_user_df['ts'] = pd.to_datetime(supp_per_user_df['ts']).dt.date
                
                if (measure_type == 'meta'):
                
                    per_user_df['gap_period'] = (per_user_df['ts'] - per_user_df['ts'].shift()).fillna(0)
                    per_user_df['gap_period'] = pd.to_numeric(per_user_df['gap_period'].astype(str).str[0])
    
                else:
                    per_user_df['eod_val'] = per_user_df[data_col_of_intrst]
    
                
                start_cntr = 0
                end_cntr = 1
                data = []
        
                for time_data in range(len(per_user_df)-1):
            
                    mask_supp = (supp_per_user_df['ts'] > per_user_df['ts'][start_cntr]) & (supp_per_user_df['ts'] <= per_user_df['ts'][end_cntr])
                    if (measure_type == 'meta'):
                        data.append([per_user_df['user_id'][0], len(supp_per_user_df.loc[mask_supp][col_name_from_supp_frame])])
                    else:
                        data.append([per_user_df['user_id'][0], supp_per_user_df.loc[mask_supp][col_name_from_supp_frame].mean()])
                
                    start_cntr += 1
                    end_cntr += 1
        
                temp_supp_frame = pd.DataFrame(data,columns = columns)
            
                temp_gap_df = pd.DataFrame()
                if (measure_type == 'meta'):
                    
                    temp_gap_df = pd.concat([temp_gap_df,per_user_df[['user_id','gap_period']]])
                else:
                    temp_gap_df = pd.concat([temp_gap_df,per_user_df[['user_id','eod_val']]])
                temp_gap_df = pd.concat([temp_gap_df, temp_supp_frame[[columns[-1]]]], axis = 1)
                
                if (measure_type == 'meta'):
                    ''' First value will have no gap period so all (eg. 85 sessions, 84 gaps) 
                    values need one shift below and first row dropped.
                    '''            
            
                    temp_gap_df[[columns[-1]]] = temp_gap_df[[columns[-1]]].shift(1)
                    temp_gap_df = temp_gap_df.drop([0]).reset_index(drop=True)
                    
                    final_temp_gap = pd.concat([final_temp_gap,temp_gap_df])
                else:
                    final_temp_gap = pd.concat([final_temp_gap,temp_gap_df])
            gapdf = pd.concat([gapdf,final_temp_gap],axis = 1)
    
        _, i = np.unique(gapdf.columns, return_index=True)
        gapdf = gapdf.iloc[:, i]
        gapdf = gapdf.reset_index(drop=True)
        gapdf = gapdf.fillna(0)
        return(gapdf)
    
    else:
        
        gapdf = pd.DataFrame()
        for user in original_frame['user_id'].unique():
    
            per_user_df = original_frame.loc[original_frame['user_id'] == user]
          
            per_user_df = per_user_df.reset_index(drop = True)
            
            if(measure_type == 'meta'):
            
                per_user_df['ts'] = pd.to_datetime(per_user_df['ts']).dt.date
           
                per_user_df['gap_period'] = (per_user_df['ts'] - per_user_df['ts'].shift()).fillna(0)
    
                per_user_df['gap_period'] = pd.to_numeric(per_user_df['gap_period'].astype(str).str[0])
         
                per_user_df = per_user_df.drop([0]).reset_index(drop=True)
            
                gapdf = pd.concat([gapdf,per_user_df[['user_id','gap_period']]])
            else:
                per_user_df['{}'.format(data_col_of_intrst)] = per_user_df[data_col_of_intrst]
                
                gapdf = pd.concat([gapdf,per_user_df[['user_id','{}'.format(data_col_of_intrst)]]])
        
        gapdf = gapdf.reset_index(drop=True)
        return(gapdf)
        




