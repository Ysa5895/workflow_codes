import pandas as pd
import numpy as np
from matplotlib import pyplot as plt
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.compose import ColumnTransformer
from sklearn.preprocessing import StandardScaler
import seaborn as sns
sns.set_style("darkgrid")
from sklearn.metrics import mean_squared_error
from math import sqrt
import statsmodels.api as sm
import matplotlib.pylab as pl
import matplotlib.gridspec as gridspec
import warnings
warnings.filterwarnings('ignore')
import os
from scipy import spatial      
import seaborn as sns
sns.set_style("darkgrid")
import warnings
warnings.filterwarnings('ignore')
from tqdm import tqdm
import time

from helper_functions import user_separator
from personalized_neighbourhood import personal_neighbourhood,  make_req_frame


'''
- Import the required files
- Remove categorical variables (question 1 and question 8)
'''

tinnitus_final_encoded = pd.read_csv(r'datasets\Tinnitus/catego_tfidf_nonenco.csv')
del tinnitus_final_encoded['question1']
del tinnitus_final_encoded['question8']


'''
Based on the session separation threshold we now have 227 long users
& 260 short users.

Import the similarity value frame
'''
 
shrt_usr_list, lng_usr_list = user_separator(tinnitus_final_encoded,
                                                              'user_id',28, 'ts')
similar_frame = pd.read_csv(r'datasets\Tinnitus/similarity_frame_28sessions_5vals_first_all_numeric_.csv')

# =============================================================================
# '''
# Remove comments only if you want save the models.
# Note: Proper implementation for its efficient usage 
# required to be done.
# '''
# 
# CACHE_PATH = r'.cached_models'
# if not os.path.exists(CACHE_PATH):
#     os.makedirs(CACHE_PATH)
# 
# =============================================================================

def average_RMSE(rmse_array, data_array):
    
    return(sum([a*b for a,b in zip(rmse_array,data_array)])/sum(data_array))
  


'''
For normalisation and model building, certain columns need to be 
ignored, so provide a list of them.

Also fix the target column
'''

ignore_for_normalization = ['user_id', 'ts']
ignore_for_model = ['user_id', 'ts']
y_variable = 'question3'


'''

Create a list of values where certain values will be stored. 

final_rmse -> Final rmse error after entire model is built
baseline_list -> Baseline RMSE (All long users used for prediction)
usr_data_points -> How many time-points were used for prediction of 
                    short-user?
no_users -> no. of long users used in that model building
neighbour_list -> the user-ids of that long users (in the order their
                                                   data was used for
                                                   model building)

'''

final_rmse = []
baseline_list = []
usr_data_points = []
no_users = []
neighbour_list = []


'''

The hyperparameters here need to be changed based on what type of
modeling we are performing. The alpha value we check for different
percentage increments, eg 0.01 imply 1%, 0.1 imply 10% and so on.

But we give a short-users list, pass similarity value frame, the actual
data frame, and other variables.
'''


for shrt_usr in tqdm(shrt_usr_list):
    
    rmse, neighbours, baseline, data_points, usr_arr = personal_neighbourhood(shrt_usr, 
                                                                              similar_frame.copy(), 
                                                                              tinnitus_final_encoded.copy(), 
                                                                              "early_termination", 
                                                                              ignore_for_normalization, 
                                                                              ignore_for_model, 
                                                                              y_variable,
                                                                              0.15)
    
    final_rmse.append(rmse[-1])
    baseline_list.append(baseline)
    usr_data_points.append(data_points)
    no_users.append(usr_arr)
    neighbour_list.append(neighbours)

'''
Find average baseline and framework RMSE. It is weighted where weights
are timepoints on which individual RMSE is calculated.
'''

average_RMSE(baseline_list,usr_data_points)


average_RMSE(final_rmse,usr_data_points)

'''
Find the training data size for each user.
'''

data_size = []
for i in range(len(neighbour_list)):
    data_size.append(len(make_req_frame(tinnitus_final_encoded, neighbour_list[i])))




